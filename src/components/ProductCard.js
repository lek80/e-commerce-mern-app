// ========== IMPORT BUILT-IN REACT MODULES ==========
import { useContext, useEffect } from 'react';

// ========== IMPORT DOWNLOADED PACKAGE MODULES ==========
import { Button, ButtonToolbar, Dropdown, DropdownButton, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

// ========== IMPORT USER-DEFINED MODULES ==========
import UserContext from '../UserContext';



// ========== CODE PROPER ==========
export default function ProductCard({ productProp }) {
	const productId = productProp._id;

	const { user } = useContext(UserContext)
	
	function archiveProduct() {
		fetch(`${process.env.REACT_APP_API_URL}/admin/product/${ productId }/status`, 
			{
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			
			if (data.message == 'Product Archived') {
				Swal.fire({
					title: "Item Archived Successfully!",
					icon: "success"
				}).then(function() {
						window.location.reload();
					}
				);

			} else {
				Swal.fire({
					title: "Error Occurred",
					icon: "error",
					html: "Oops! Something went wrong..."
				});
			}
		})
	}

	function activateProduct() {
		fetch(`${process.env.REACT_APP_API_URL}/admin/product/${ productId }/status`, 
			{
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			
			if (data.message == 'Product Activated') {
				Swal.fire({
					title: "Item Activated Successfully!",
					icon: "success"
				}).then(function() {
						window.location.reload();
					}
				);

			} else {
				Swal.fire({
					title: "Error Occurred",
					icon: "error",
					html: "Oops! Something went wrong..."
				});
			}
		})
	}

	function addToCart(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/user/product/${ productId }`)
		.then(res => res.json())
		.then(data => {

			console.log(data.name);

			fetch(`${process.env.REACT_APP_API_URL}/user/product/${ productId }`,
				{
					method: "POST",
					headers: {
						"Content-Type": "application/json",
						"Authorization": `Bearer ${ localStorage.getItem('token') }`
					},
					body: JSON.stringify({
						items: [
							{
								productId: productId,
								name: data.name,
								description: data.description,
								category: data.category,
								price: data.price,
								quantity: 1
							}
						]
					})
				})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				if (data.message == "Added to Cart") {
					Swal.fire({
						title: "Added to Cart!",
						icon: "success",
						text: "You have successfully added the item/s to your cart"
					});

				} else if (data.message == "Item Already in Cart") {

					Swal.fire({
						title: "Item Already Added",
						icon: "error",
						text: "Please proceed to your cart to edit its quantity"
					});

				} else if (data.message == "Added Item") {

					Swal.fire({
						title: "Added to Cart!",
						icon: "success",
						text: "You have successfully added the item/s to your cart"
					});

				}
			});
		});
	}

	return (
				<Card>
			     <Card.Body>
			       <Card.Title>
			       	<h5>{ productProp.name }</h5>
			       </Card.Title>
			       <Card.Subtitle>Price</Card.Subtitle>
			       <Card.Text>{ productProp.price }</Card.Text>

			       {
			       	user.isAdmin ? 
			       		<>
			       			<Card.Subtitle>Description</Card.Subtitle>
			       			<Card.Text>{ productProp.description }</Card.Text>
			       			<Card.Subtitle>Category</Card.Subtitle>
			       			<Card.Text>{ productProp.category }</Card.Text>
			       	       <Card.Subtitle>Status</Card.Subtitle>
			       	       <Card.Text>{ productProp.isActive ? 
			       	       		<span style={{ color: "green" }}>Active</span> : <span style={{ color: "red" }}>Not Active</span>  
			       	       			}
			       	       </Card.Text>

			       	       <ButtonToolbar>
			       		       	<Button 
			       		       	variant="success" 
			       		       	as={ Link } 
			       		       	to={"/admin/product/update/" + productProp._id}>
			       		       	Edit
			       		       	</Button>

			       		       	<DropdownButton 
			       			       	title="Toggle Status" 
			       			       	variant="outline-success" 
			       			       	className="mx-1">
			       		       	        <Dropdown.Item onClick={ activateProduct }>Activate</Dropdown.Item>
			       		       	        <Dropdown.Item onClick={ archiveProduct }>Archive</Dropdown.Item>
			       		       	</DropdownButton>

			       		      	<Button 
			       		      	variant="danger">
			       		      	Delete</Button>
			       		   </ButtonToolbar>
			       		</>

			       	: user.isAdmin == null ? 

			       			<Button 
			       			variant="warning"
			       			as={ Link }
			       			to={ "/user/product/" + productProp._id }>
			       			Check Details
			       			</Button> 

			       		:  <ButtonToolbar>
				       			<Button 
				       			variant="secondary"
				       			as={ Link }
				       			to={ "/user/product/" + productProp._id }>
				       			Check Details
				       			</Button>

				       			<Button 
				       			variant="warning" 
				       			className="mx-1"
				       			onClick={ addToCart }>
				       			Add to Cart
				       			</Button>
			       			</ButtonToolbar>
			       }
			       
			     </Card.Body>
			   </Card>
	)
}
