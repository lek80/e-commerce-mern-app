// ========== IMPORT BUILT-IN REACT MODULES ==========
import { useState, useEffect, useContext } from 'react';

// ========== IMPORT DOWNLOADED PACKAGE MODULES ==========
import { Row, Col, Form, Button } from 'react-bootstrap';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

// ========== IMPORT USER-DEFINED MODULES ==========
import UserContext from '../UserContext';



// ========== CODE PROPER ==========
export default function Login() {

	const { user, setUser } = useContext(UserContext);
	const navigate = useNavigate();

	const [loginUsername, setLoginUsername] = useState('');
	const [loginEmail, setLoginEmail] = useState('');
	const [loginPassword, setLoginPassword] = useState('');
	const [status, setStatus] = useState(true);

	console.log(process.env.REACT_APP_API_URL);


	useEffect(() => {
		if (loginUsername !== '' && loginPassword !== '') {
			setStatus(false);
		} else {
			setStatus(true);
		}

	}, [loginUsername, loginPassword]);

	function loginUser(e) {
	
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/user/login`, 
		{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				username: loginUsername,
				password: loginPassword
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);
	
			if (typeof data.access !== "undefined") {

				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Sign-in Successful",
					icon: "success",
					text: "Welcome back to React!"
				});
			} else {

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your sign-in credentials and try again."
				});
			}
		});

		setLoginUsername('');
		setLoginPassword('');
	}

	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
				headers: {
					Authorization: `Bearer ${ token }`
				}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}		

	return (

		(user.isAdmin == true) ?
			<Navigate to="/admin/product/all" />
			:
				(user.id !== null) ?
					<Navigate to="/" />
				:
					<>
						<Row>
							<Col className="text-center py-5">
								<h1>Welcome to React! Please sign in.</h1>
							</Col>
						</Row>

						<Form onSubmit={ loginUser }>
					      <Form.Group className="mb-3" controlId="email">
					        <Form.Label>Username</Form.Label>
					        <Form.Control 
					        	type="text" 
					        	placeholder="Enter username" 
					        	value={ loginUsername }
					        	onChange={ e => setLoginUsername(e.target.value) }
					        	required />
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="password">
					        <Form.Label>Password</Form.Label>
					        <Form.Control 
					        	type="password" 
					        	placeholder="Password"
					        	value={ loginPassword }
					        	onChange={ e => setLoginPassword(e.target.value) } 
					        	required />
					      </Form.Group>

					      <Row>
					      	<Col>
						      	<Button 
						      		variant="warning" 
						      		type="submit" 
						      		id="submitBtn"
						      		disabled= { status ? true : false }>
						      		  SIGN IN
						      	</Button>
						    </Col>
						    <Col className="d-inline-block col-4">
						      	<p>
						      		Don't have an account yet? <Link to="/user/register" style={{ textDecoration: "none" }}>Click here</Link> to register.
						      	</p>
					      	</Col>
					  	  </Row>
					    </Form>
					</>
	);
}