// ========== IMPORT BUILT-IN REACT MODULES ==========
import { useEffect, useContext } from 'react';

// ========== IMPORT DOWNLOADED PACKAGE MODULES ==========
import { Navigate } from 'react-router-dom';

// ========== IMPORT USER-DEFINED MODULES ==========
import UserContext from '../UserContext';



// ========== CODE PROPER ==========
export default function Logout() {

	const { unsetUser, setUser } = useContext(UserContext);

	unsetUser();

	useEffect(() => {

		setUser({ id: null })
	})

	return (

		<Navigate to='/user/login' />
	)
}