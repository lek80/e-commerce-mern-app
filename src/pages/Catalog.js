// ========== IMPORT BUILT-IN REACT MODULES ==========
import { useEffect, useState } from 'react';

// ========== IMPORT USER-DEFINED MODULES ==========
import ProductCard from '../components/ProductCard';



// ========== CODE PROPER ==========
export default function Catalog() {

	const [products, setProducts] = useState([]);

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/user/product/all`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

				setProducts(data.map(product => {

				return (
					<ProductCard key={ product._id } productProp={ product } />
				)
			}));
		})
	}, []);

	return (
		<>
			{ products }
		</>
	)
}